import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AppbarComponent } from "./appbar/appbar.component";
import { TextComponent } from './text/text.component';
import { ActionsComponent } from './actions/actions.component';
import { IconComponent } from './icon/icon.component';
import { ButtonComponent } from './button/button.component';

@NgModule({
  declarations: [
    AppbarComponent,
    TextComponent,
    ActionsComponent,
    IconComponent,
    ButtonComponent,
  ],
  exports: [
    AppbarComponent,
    TextComponent,
    ActionsComponent,
    IconComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MaterialModule {}