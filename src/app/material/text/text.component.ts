import { Component, Input } from '@angular/core';

@Component({
  selector: 'text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent {
  @Input() weight: Weight = 100;
  @Input() color: string = "#000000";
  @Input() type: TextType = "span";
  @Input() fontSize: string = "1em";
}

export type Weight = 100 | 300 | 400 | 500 | 700 | 900;
export type TextType = "h1" | "h2" | "h3" | "span";